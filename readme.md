This is a hack to BookStack, using the theme system, so that login presents itself as a username.
Upon login attempt, this will match to a user of `<username>@<configured-domain>` within the database.

### Setup

This uses the [logical theme system](https://github.com/BookStackApp/BookStack/blob/development/dev/docs/logical-theme-system.md).

1. Within the BookStack install folder, you should have a `themes` folder.
2. Create a `themes/custom/functions.php` file with the contents of the `functions.php` file example below.
3. Configure the `EMAIL_DOMAIN` to be your desired email domain (Should be common across BookStack users).
4. Create a `themes/custom/auth/parts/login-form-standard.blade.php` file with the contents of the `login-form-standard.blade.php` file below.
5. Add `APP_THEME=custom` to your .env file.

### Note

These customizations are not officially supported any may break upon, or conflict with, future updates. 
Quickly tested on BookStack v22.10